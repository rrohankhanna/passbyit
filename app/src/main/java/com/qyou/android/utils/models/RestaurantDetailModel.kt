package com.qyou.android.utils.models

import java.io.Serializable

data class RestaurantDetailModel(
    val detail: Detail?=null,
    val status: Int?=null,
    val url: String?=null
):Serializable{
    data class Detail(
        val access_token: String,
        val active_tables: ArrayList<ActiveTable>,
        val address: String,
        val available_seats: Int,
        val city: String,
        val country: String,
        val created_by_id: Any,
        val created_on: String,
        val email: String,
        val first_name: String,
        val full_name: String,
        val id: Int,
        val inactive_tables: ArrayList<InactiveTable>,
        val is_notify: Int,
        val language: String,
        val last_action_time: Any,
        val last_name: String?,
        val last_password_change: Any,
        val last_visit_time: String,
        val latitude: String,
        val login_error_count: Any,
        val logo_file: String,
        val longitude: String,
        val profile_file: String,
        val qr_file: String,
        val role_id: Int,
        val state_id: Int,
        val timezone: String,
        val tos: Any,
        val type_id: Int,
        val zipcode: String,
        val estimated_waiting_time:String?=null
    ):Serializable{
        data class ActiveTable(
            val created_by_id: Int,
            val created_on: String,
            val id: Int,
            val minimum_bid: String,
            val state_id: Int,
            val table_name: String,
            val type_id: Any,
            val updated_on: String
        ):Serializable

        data class InactiveTable(
            val created_by_id: Int,
            val created_on: String,
            val id: Int,
            val minimum_bid: Any,
            val state_id: Int,
            val table_name: String,
            val type_id: Any,
            val updated_on: String
        ):Serializable
    }
}