package com.qyou.android.utils.models

data class AddFavouriteModel(
    val message: String,
    val status: Int,
    val url: String
)