package com.qyou.android.utils.models

import java.io.Serializable

data class CasinoQueueModel(
    val detail: Detail,
    val message: String,
    val status: Int,
    val url: String
) :Serializable{
    data class Detail(
        var created_by_id: String?=null,
        var created_on: String?=null,
        var estimated_time: Any?=null,
        var group_person: Any?=null,
        var id: Int?=null,
        var pending_users: String?=null,
        var queue_no: String?=null,
        var restaurant_id: Int?=null,
        var session_id: String?=null,
        var state_id: Int?=null,
        var total_registered_users: String?=null,
        var type_id: Int?=null,
        var message:String?=null,
        var name:String?=null,
        var image:String?=null,
        var logo:String?=null
    ):Serializable
}