package com.qyou.android.utils

import android.icu.number.NumberRangeFormatter.with
import android.util.Log
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.makeramen.roundedimageview.RoundedImageView
import com.qyou.android.R
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

object CustomBindingAdapters {

    @BindingAdapter(value = ["setRecyclerAdapter"], requireAll = false)
    @JvmStatic
    fun setRecyclerAdapter(
        recyclerView: RecyclerView,
        adapter: RecyclerView.Adapter<*>
    ) {
        if (adapter != null) {
            recyclerView.adapter = adapter
        }
    }


    @BindingAdapter(value = ["setCircleImage"],requireAll = false)
    @JvmStatic
    fun setCircleImage(imageView: CircleImageView, path: String?) {
        if(!path.isNullOrEmpty())
            Picasso.get().load(path).placeholder(R.color.grey).into(imageView)

    }

    @BindingAdapter(value = ["setBannerImage"],requireAll = false)
    @JvmStatic
    fun setBannerImage(imageView: ImageView, path: String?) {
        if(!path.isNullOrEmpty())
            Picasso.get().load(path).placeholder(R.color.grey).into(imageView)

    }

    @BindingAdapter(value = ["setRoundImage"],requireAll = false)
    @JvmStatic
    fun setRoundImage(roundedImageView: RoundedImageView, path: String?) {
        if(!path.isNullOrEmpty())
            Picasso.get().load(path).placeholder(R.color.grey).into(roundedImageView)

    }


}