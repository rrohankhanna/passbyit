package com.qyou.android.utils.models

data class CancelBookingModel(
    val message: String,
    val error: String,
    val status: Int,
    val url: String
)