package com.qyou.android.utils.models

data class GetQueueDetailModel(
    val detail: Detail,
    val status: Int,
    val url: String
) {
    data class Detail(
        val created_by_id: Int,
        val created_on: String,
        val estimated_time: Any,
        val group_person: String,
        val id: Int,
        val pending_users: String,
        val queue_no: String,
        val restaurant_id: Int,
        val session_id: String,
        val state_id: Int,
        val total_registered_users: String,
        val type_id: Any
    )
}