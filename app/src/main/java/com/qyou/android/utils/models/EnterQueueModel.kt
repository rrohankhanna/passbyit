package com.qyou.android.utils.models

import java.io.Serializable

data class EnterQueueModel(
    val detail: Detail,
    val message: String,
    val status: Int,
    val url: String
) :Serializable{
    data class Detail(
        val created_by_id: Int?=null,
        val created_on: String?=null,
        val estimated_time: Any?=null,
        val group_person: Any?=null,
        var id: Int?=null,
        var pending_users: String?=null,
        var queue_no: String?=null,
        val restaurant_id: Int?=null,
        val session_id: String?=null,
        val state_id: Int?=null,
        var total_registered_users: String?=null,
        val type_id: Any?=null,
        var name:String?=null,
        var image:String?=null,
        var logo:String?=null,
        var groupNumber:String?=null,
        var type:String?=null,
        var title:String?=null,
        var message:String?=null,
        var fromNotification:Boolean = false
    ):Serializable
}