package com.qyou.android.utils.WebClientSetup

import com.qyou.android.utils.WebClientSetup.RetrofitAPIs


interface RequestProcess <T> {

    suspend fun sendRequest(retrofitApi: RetrofitAPIs):T

    fun onResponse (res : T)

    fun onException (message : String)

}