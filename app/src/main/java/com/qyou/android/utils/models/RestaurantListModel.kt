package com.qyou.android.utils.models

data class RestaurantListModel(
    val _links: Links,
    val _meta: Meta,
    val list: ArrayList<RestaurantDetailModel.Detail>,
    val status: Int,
    val url: String
) {
    data class Links(
        val first: First,
        val last: Last,
        val self: Self
    ) {
        data class First(
            val href: String
        )

        data class Last(
            val href: String
        )

        data class Self(
            val href: String
        )
    }

    data class Meta(
        val currentPage: Int,
        val pageCount: Int,
        val perPage: Int,
        val totalCount: Int
    )

    data class RestClass(
        val access_token: String,
        val active_tables: ArrayList<TableModel>,
        val address: String,
        val available_seats: Int,
        val city: String,
        val country: String,
        val created_by_id: Any,
        val created_on: String,
        val email: String,
        val estimated_waiting_time: String,
        val first_name: Any,
        val full_name: String,
        val id: Int,
        val inactive_tables: List<TableModel>,
        val is_notify: Int,
        val language: String,
        val last_action_time: Any,
        val last_name: Any,
        val last_password_change: Any,
        val last_visit_time: String,
        val latitude: String,
        val login_error_count: Any,
        val logo_file: String,
        val longitude: String,
        val profile_file: String,
        val qr_file: String,
        val role_id: Int,
        val state_id: Int,
        val timezone: String,
        val tos: Any,
        val type_id: Int,
        val zipcode: String
    ) {


        data class TableModel(
            val created_by_id: Int,
            val created_on: String,
            val id: Int,
            val minimum_bid: Any,
            val state_id: Int,
            val table_name: String,
            val total_seats: Int,
            val type_id: Any,
            val updated_on: String
        )
    }
}