package com.qyou.android.utils.models

data class PendingRequestModel(
    val list: ArrayList<PendingList>,
    val status: Int,
    val url: String,
    val error: String?=null
) {
    data class PendingList(
        val created_by_id: Int,
        val created_on: String,
        val estimated_time: Any,
        val group_person: String,
        val id: Int,
        val pending_users: String,
        val queue_no: String,
        val restaurant: Restaurant,
        val restaurant_id: Int,
        val session_id: String,
        val state_id: Int,
        val total_registered_users: String,
        val type_id: Any
    ) {
        data class Restaurant(
            val access_token: String,
            val address: String,
            val available_seats: Int,
            val city: Any,
            val country: Any,
            val created_by_id: Any,
            val created_on: String,
            val email: String,
            val estimated_waiting_time: String,
            val first_name: Any,
            val full_name: String,
            val id: Int,
            val is_notify: Int,
            val language: Any,
            val last_action_time: Any,
            val last_name: Any,
            val last_password_change: Any,
            val last_visit_time: String,
            val latitude: String,
            val login_error_count: Any,
            val logo_file: String,
            val longitude: String,
            val profile_file: String,
            val qr_file: String,
            val qr_scan_message: Any,
            val role_id: Int,
            val state_id: Int,
            val timezone: Any,
            val tos: Any,
            val total_registered_users: String,
            val type_id: Int,
            val zipcode: Any
        )
    }
}