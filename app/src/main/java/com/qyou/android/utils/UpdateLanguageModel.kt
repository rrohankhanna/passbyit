package com.qyou.android.utils

data class UpdateLanguageModel(
    val status: Int,
    val url: String,
    val message: String
)