package com.qyou.android.utils.WebClientSetup

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.qyou.android.R
import com.qyou.android.utils.WebClientSetup.RequestProcess
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClientBuilder {
    fun <T> callService(context:Context,
        dialogFlag: Boolean,
        token: String?,
        requestProcessor: RequestProcess<T>
    ) {
        val interceptor = HttpLoggingInterceptor{message->
            Log.e("","Log: $message")
        }
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        try {
            if (dialogFlag)
                showDialog(context)

            var okHttpClient: OkHttpClient? = null
            if (token.isNullOrEmpty()) {
                okHttpClient = OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.MINUTES)
                    .connectTimeout(10, TimeUnit.MINUTES)
                    .addInterceptor(interceptor)
                    .build()
            } else {
                val client = OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.MINUTES)
                    .connectTimeout(10, TimeUnit.MINUTES)
                    .addInterceptor(interceptor)
                    .addInterceptor { chain ->
                        val original = chain.request()

                        val request = original.newBuilder()
                            .addHeader("Authorization", token)
                            .method(original.method, original.body)
                            .build()
                        chain.proceed(request)
                    }
                okHttpClient = client.build()

            }
            val retrofit = Retrofit.Builder()
                .baseUrl(WebUrls.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()


            val retrofitApi = retrofit.create(RetrofitAPIs::class.java)

            val coRoutineExceptionHandler = CoroutineExceptionHandler { _, t ->
                t.printStackTrace()

                CoroutineScope(Dispatchers.Main).launch {
                    hideDialog()
                    t.message?.let { requestProcessor.onException(it) }
                }
            }

            CoroutineScope(Dispatchers.IO + coRoutineExceptionHandler).launch {

                val response = requestProcessor.sendRequest(retrofitApi) as Response<*>

                CoroutineScope(Dispatchers.Main).launch {
                    hideDialog()
                    if (response.isSuccessful)
                        requestProcessor.onResponse(response as T)
                    else{
                        val res = response.errorBody()!!.string()
                        requestProcessor.onException(res)
                    } }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } catch (e: Throwable) {
            Log.e("TAG", "Server Error  $e")
        }
    }
    var customDialog : AlertDialog? = null


    private fun showDialog(mContext: Context){
        val customAlertBuilder = AlertDialog.Builder(mContext)
        val customAlertView =
            LayoutInflater.from(mContext).inflate(R.layout.progress_dialog, null)
        customAlertBuilder.setView(customAlertView)
        customAlertBuilder.setCancelable(false)
        customDialog = customAlertBuilder.create()
        customDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        customDialog!!.show()
    }


    private fun hideDialog() {
        if (customDialog != null) {
            customDialog!!.dismiss()
        }
    }

}