package com.qyou.android.utils

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessaging
//import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.qyou.android.R
import com.qyou.android.views.FragmentLoadActivity
import com.qyou.android.views.MainActivity
import com.qyou.android.views.StatusActivity
import org.json.JSONObject

class FirebaseMessaging : FirebaseMessagingService() {
    private var TAG = "FirebaseMessaging"
    var builder: Notification.Builder? = null
    var intent: Intent? = null
    var type = ""
    var message = ""
    var description = ""
    var title = ""
    var jsonObject: JSONObject? = null

    var type_id = ""
    var model_id = ""
    var created_by_id = ""

    var mBuilder: NotificationCompat.Builder? = null
    var notificationManager: NotificationManager? = null
    var notification: Notification? = null


    override fun onNewToken(s: String) {
        super.onNewToken(s)
        FirebaseMessaging.getInstance().token.addOnCompleteListener {
        }
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        Log.e("notificationData", "$----------->${p0.notification}   ${p0.data}}")
        Log.e("NotificationOnly", p0.notification.toString())

        p0.data.isNotEmpty().let {

            val messageBody = p0.data

            Log.e("MessageBody", messageBody.toString())
            var json = JSONObject(Gson().toJson(messageBody))

            //message = messageBody["message"].toString()
            title = messageBody["title"].toString()
            description = messageBody["body"].toString()
            type_id = messageBody["type_id"].toString()
            model_id = messageBody["model_id"].toString()
            created_by_id = messageBody["created_by_id"].toString()

        }

        showNotification(this, title, description)



    }

    private fun showNotification(
        context: Context,
        title: String?,
        message: String?
    ) {

        Log.e("flagg", checkActivation(applicationContext).toString())

        try {

            if (checkActivation(applicationContext)) {
                intent = Intent(applicationContext, FragmentLoadActivity::class.java)
                intent!!.putExtra("type_id",type_id)
                intent!!.putExtra("created_by_id",created_by_id)
                intent!!.putExtra("model_id",model_id)
                intent!!.putExtra("title",title)
                intent!!.putExtra("body",message)

                LocalBroadcastManager.getInstance(applicationContext)
                    .sendBroadcast(intent!!)

            } else {
                intent = Intent(applicationContext, FragmentLoadActivity::class.java)
                intent!!.putExtra("type_id",type_id)
                intent!!.putExtra("created_by_id",created_by_id)
                intent!!.putExtra("model_id",model_id)
                intent!!.putExtra("title",title)
                intent!!.putExtra("body",message)

                LocalBroadcastManager.getInstance(applicationContext)
                    .sendBroadcast(intent!!)
            }

            intent!!.addFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP or
                        Intent.FLAG_ACTIVITY_CLEAR_TASK or
                        Intent.FLAG_ACTIVITY_NEW_TASK
            )

            val pendingIntent = PendingIntent.getActivity(
                this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT
            )

            mBuilder = NotificationCompat.Builder(this)
            notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            /*val uri =
                Uri.parse("android.resource://" + context.packageName + "/" + R.raw.notification_tone)*/
            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION).build()


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                var mChannel: NotificationChannel? = null

                if (notificationManager != null) {
                    val channelList = notificationManager!!.notificationChannels
                    var i = 0
                    while (channelList != null && i < channelList.size) {
                        notificationManager!!.deleteNotificationChannel(channelList[i].id)
                        i++
                    }
                }
                mChannel =
                    NotificationChannel("default", message, NotificationManager.IMPORTANCE_HIGH)
                //mChannel.setSound(uri, attributes)


                notification = mBuilder!!
                    .setWhen(0)
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.qyou_logo)
                    //.setColor(ContextCompat.getColor(this, R.color.red_color))
                    .setPriority(Notification.PRIORITY_MAX)
                    //.setSound(uri)
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(message)
                    )
                    .setContentText(message)
                    .setChannelId("default")
                    .build()

                notification!!.defaults = 0
                //notification!!.sound = uri

                notificationManager!!.createNotificationChannel(mChannel)

                notificationManager!!.notify(1, notification)

            } else {
                val notificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                @SuppressLint("ResourceAsColor") val notificationBuilder =

                    mBuilder!!
                        .setWhen(0)
                        .setAutoCancel(true)
                        .setContentTitle(title)
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(R.drawable.qyou_logo)
                        //.setColor(ContextCompat.getColor(this, R.color.red_color))

                        .setStyle(NotificationCompat.BigTextStyle().bigText(message))
                        .setPriority(Notification.PRIORITY_MAX)
                        //.setSound(uri)
                        .setContentText(message)
                        .build()
                notificationManager.notify(1, notificationBuilder)
            }


        } catch (e: Exception) {

            e.printStackTrace()
        }

    }


    private fun getNotificationIcon(
        notificationBuilder: NotificationCompat.Builder,
        context: Context
    ): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val color = ContextCompat.getColor(context, R.color.app_color)
            notificationBuilder.color = color
            R.drawable.logo
        } else {

            R.drawable.logo
        }
    }


    private fun checkActivation(context: Context): Boolean {
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val services = activityManager.getRunningTasks(Integer.MAX_VALUE)
        var isActivityFound = false
        if (services[0].topActivity!!.packageName.toString()
                .equals(context.packageName.toString(), ignoreCase = true)
        ) {
            isActivityFound = true
        }
        Log.e(" FCM ", "Activity open: $isActivityFound")  // true foreground n false background
        return isActivityFound
    }
}