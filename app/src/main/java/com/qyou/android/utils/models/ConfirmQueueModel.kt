package com.qyou.android.utils.models

data class ConfirmQueueModel(
    val status: Int,
    val url: String
)