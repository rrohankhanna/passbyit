package com.qyou.android.utils.WebClientSetup

object WebUrls {

    const val BASE_URL = "https://passby.to/api/"

    const val GET_RESTAURANT_DETAIL = "user/get-restaurant-detail"
    const val EDIT_DETAIL = "user/get-casino-queue"
    const val GET_QUEUE_DETAIL = "user/get-queue-detail"
    const val START_QUEUE = "user/start-queue"
    const val START_GROUP_QUEUE = "user/start-queue"
    const val RESTAURANT_LIST = "user/restaurant-list"
    const val GET_PENDING_REQUEST = "user/active-queue-list"
    const val CANCEL_BOOKING = "user/cancel-booking"
    const val CONFIRM_QUEUE = "user/confirm-queue"
    const val GET_LONG_URL = "user/get-long-url"
    const val CHANGE_LANGUAGE = "user/update-user-language"
    const val FAVOURITE_LIST = "user/favourite-list"
    const val NOTIFICATION_LIST = "user/notification-list"
    const val ADD_FAVOURITE = "user/add-favourite"



    const val LANGUAGE_KEY = "lang_key"
}