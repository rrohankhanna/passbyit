package com.qyou.android.utils.WebClientSetup
import com.qyou.android.utils.UpdateLanguageModel
import com.qyou.android.utils.models.*
import retrofit2.Response
import retrofit2.http.*

interface RetrofitAPIs {

    @GET(WebUrls.GET_RESTAURANT_DETAIL)
    suspend fun getRestaurantDetail(@Query("id")id:String,@Query ("user_token")token:String):Response<GetCasinoDetailModel>

    @FormUrlEncoded
    @POST(WebUrls.EDIT_DETAIL)
    suspend fun editDetail(@Query ("id") id:String,@FieldMap hashMap: HashMap<String,Any>):Response<CasinoQueueModel>

    @FormUrlEncoded
    @POST(WebUrls.RESTAURANT_LIST)
    suspend fun getRestaurantList(@FieldMap hashMap: HashMap<String, Any>):Response<RestaurantListModel>


    @GET(WebUrls.GET_QUEUE_DETAIL)
    suspend fun getQueueDetail(@Query("id")id:String):Response<GetQueueDetailModel>

    @FormUrlEncoded
    @POST(WebUrls.START_QUEUE)
    suspend fun startQueue(@Query ("id") id:String,@Field("session_id")session_id:String,@Field("device_type")type:String):Response<EnterQueueModel>

    @FormUrlEncoded
    @POST(WebUrls.START_QUEUE)
    suspend fun startGroupQueue(@Query ("id") id:String,@Query("group")group:String,@Field("session_id")session_id:String,@Field("device_type")type:String):Response<EnterQueueModel>

    @FormUrlEncoded
    @POST(WebUrls.CANCEL_BOOKING)
    suspend fun cancelBooking(@Query ("id") id:String,@Field("OrderQueue[cancel_reason]")reason:String):Response<CancelBookingModel>


    @GET(WebUrls.GET_PENDING_REQUEST)
    suspend fun getPendingRequest(@Query("device_token")id:String):Response<PendingRequestModel>

    @GET(WebUrls.CONFIRM_QUEUE)
    suspend fun confirmQueueModel(@Query("id")id:String):Response<ConfirmQueueModel>

    @FormUrlEncoded
    @POST(WebUrls.GET_LONG_URL)
    suspend fun getLongUrl(@Field ("url") url:String):Response<GetLongUrlModel>


    @FormUrlEncoded
    @POST(WebUrls.CHANGE_LANGUAGE)
    suspend fun changeLanguage(@Field ("language") language:String,@Field("device_token") device_token:String):Response<UpdateLanguageModel>


    @FormUrlEncoded
    @POST(WebUrls.FAVOURITE_LIST)
    suspend fun getFavouriteList(@Field ("user_token") user_token:String,@Field("page") page:String):Response<FavouriteListModel>


    @FormUrlEncoded
    @POST(WebUrls.NOTIFICATION_LIST)
    suspend fun getNotificationList(@Field ("user_token") user_token:String,@Field("page") page:String):Response<NotificationListModel>


    @FormUrlEncoded
    @POST(WebUrls.ADD_FAVOURITE)
    suspend fun addToFavourite(@Field ("Favourite[restaurant_id]") id:String,@Field("Favourite[user_token]") user_token:String):Response<AddFavouriteModel>


}