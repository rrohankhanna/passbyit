package com.qyou.android.utils.models

data class NotificationListModel(
    val _links: Links,
    val _meta: Meta,
    val list: ArrayList<NotificationList>,
    val status: Int,
    val url: String
) {
    data class Links(
        val first: First,
        val last: Last,
        val self: Self
    ) {
        data class First(
            val href: String
        )

        data class Last(
            val href: String
        )

        data class Self(
            val href: String
        )
    }

    data class Meta(
        val currentPage: Int,
        val pageCount: Int,
        val perPage: Int,
        val totalCount: Int
    )

    data class NotificationList(
        val created_by_id: Int,
        val created_on: String,
        val description: String,
        val id: Int,
        val is_read: Int,
        val model_id: Int,
        val model_type: String,
        val state_id: Int,
        val title: String,
        val to_user_id: Any,
        val type_id: Int
    )
}