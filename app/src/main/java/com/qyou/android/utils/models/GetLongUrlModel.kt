package com.qyou.android.utils.models

data class GetLongUrlModel(
    val error: String,
    val status: Int,
    val returnUrl: String,
    val message: String
)