package com.qyou.android.utils

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.provider.Settings
import android.text.format.Formatter
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.qyou.android.R
import com.qyou.android.utils.WebClientSetup.WebUrls
import kotlinx.android.synthetic.main.choose_language.view.*
import kotlinx.android.synthetic.main.confirm_queue_alert.view.*
import java.text.SimpleDateFormat

object CommonMethods {

    fun isNetworkAvailable(context: Context): Boolean {

        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val wm = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val ip = Formatter.formatIpAddress(wm.connectionInfo.ipAddress)

        val wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        if (wifi != null && wifi.isConnected) {
            return true
        }

        val mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        if (mobile != null && mobile.isConnected) {
            return true
        }

        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    fun getDeviceId(context: Context):String{
        return Settings.Secure.getString(context.contentResolver,
            Settings.Secure.ANDROID_ID);
    }


    fun showToast(context: Context,message:String){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
    }

    fun convertTimeTo24Hr(time: String): String {
        val to = SimpleDateFormat("HH:mm")
        val from = SimpleDateFormat("hh:mm aa")
        val date1 = from.parse(time)
        return to.format(date1)
    }

    fun openAlert(context: Context,message: String){
        var customDialog: androidx.appcompat.app.AlertDialog? = null
        val customAlertBuilder = androidx.appcompat.app.AlertDialog.Builder(context)

        val customAlertView =
            LayoutInflater.from(context).inflate(R.layout.confirm_queue_alert, null)
        customAlertBuilder.setView(customAlertView)
        //alertDialogBuilder.setTitle(context.getString(R.string.app_name))
        customDialog = customAlertBuilder.create()
        customDialog?.show()

        customDialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        customDialog?.setCancelable(false)
        customDialog?.setCanceledOnTouchOutside(false)
        customAlertView.tvMessage.text = message
        customAlertView.tvYes.text = "OK"
        customAlertView.tvNo.visibility = View.GONE

        customAlertView.tvYes.setOnClickListener {
            customDialog.dismiss()
        }
    }
}