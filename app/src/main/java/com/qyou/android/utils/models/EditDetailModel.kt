package com.qyou.android.utils.models

data class EditDetailModel(
    val detail: Detail,
    val message: String,
    val status: Int,
    val error: String,
    val url: String
) {
    data class Detail(
        val created_by_id: String,
        val created_on: String,
        val estimated_time: String,
        val id: Int,
        val queue_no: Int,
        val restaurant_id: Int,
        val session_id: String,
        val state_id: Int,
        val type_id: Any
    )
}