package com.qyou.android.views.restaurantList

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModel
import com.qyou.android.utils.models.RestaurantDetailModel
import com.qyou.android.utils.models.RestaurantListModel
import com.qyou.android.views.detail.DetailActivity

class RestaurantListAdapterVM(var context: Context, val restClass: RestaurantDetailModel.Detail):ViewModel() {


    fun onClickRestaurant(view:View){
        val bundle = Bundle()
        bundle.putSerializable("restaurant_detail",restClass)
        context.startActivity(Intent(context, DetailActivity::class.java).putExtras(bundle))
    }

}