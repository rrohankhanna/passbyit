package com.qyou.android.views

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.qyou.android.R
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.models.CancelBookingModel
import com.qyou.android.utils.models.CasinoQueueModel
import com.qyou.android.utils.models.GetQueueDetailModel
import kotlinx.android.synthetic.main.cancel_alert.view.*
import retrofit2.Response

class StatusActivityVM(var context: Context,var statusActivity: StatusActivity):ViewModel() {

    var isThanks = ObservableBoolean(false)
    var isWaiting = ObservableBoolean(true)
    var isRejected = ObservableBoolean(false)
    var isAccepted = ObservableBoolean(false)
    var casinoQueueModel : CasinoQueueModel.Detail?=null
    var statusModel = StatusModel()
    var counter : CountDownTimer?=null


    init {
        getBundleData()
    }

    private fun getBundleData() {
        casinoQueueModel = statusActivity.intent.extras!!.getSerializable("casino_model") as CasinoQueueModel.Detail
        statusModel.restName.set(casinoQueueModel?.name)
        statusModel.message.set(casinoQueueModel?.message)
        statusModel.banner.set(casinoQueueModel?.image)
        statusModel.total_user.set(casinoQueueModel?.total_registered_users)
        statusModel.position.set(casinoQueueModel?.queue_no)
        statusModel.logo.set(casinoQueueModel?.logo)
        startTimeCounter()
    }





    fun onClickCancel(view: View){
        var customDialog: androidx.appcompat.app.AlertDialog? = null
        val customAlertBuilder = androidx.appcompat.app.AlertDialog.Builder(context)

        val customAlertView =
            LayoutInflater.from(context).inflate(R.layout.cancel_alert, null)
        customAlertBuilder.setView(customAlertView)
        //alertDialogBuilder.setTitle(context.getString(R.string.app_name))
        customDialog = customAlertBuilder.create()
        customDialog?.show()

        customDialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        customDialog?.setCancelable(true)
        customDialog?.setCanceledOnTouchOutside(true)

        customAlertView.btnCancel.setOnClickListener {
            if (customAlertView.et_reason.text.trim().isNotEmpty()) {
                cancelBookingApi(customAlertView.et_reason.text.trim().toString())
            } else {
                CommonMethods.showToast(context, context.getString(R.string.please_enter_reason_for_cancellation))
            }
        }

    }

    private fun cancelBookingApi(reason:String) {
        RetrofitClientBuilder.callService(context, true, "", object :
            RequestProcess<Response<CancelBookingModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<CancelBookingModel> {
                return retrofitApi.cancelBooking(casinoQueueModel?.id!!.toString(),reason)
            }

            override fun onResponse(res: Response<CancelBookingModel>) {

                if (res.body()?.status == 200) {
                    CommonMethods.showToast(context,res.body()?.message!!)
                    context.startActivity(Intent(context, FragmentLoadActivity::class.java))
                    (context as Activity).finishAffinity()

                }
                else{
                    CommonMethods.showToast(context,res.body()?.error!!)
                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context, "Something went wrong.")
            }

        })
    }








    private  fun startTimeCounter() {
        counter = object : CountDownTimer(3000, 300) {
            override fun onTick(millisUntilFinished: Long) {
            }
            override fun onFinish() {
                Log.e("sdsdfsdf","sjdfsbjf")
                getQueueDetail()
            }
        }.start()
    }


    private fun getQueueDetail() {
        RetrofitClientBuilder.callService(context,false,"",object :
            RequestProcess<Response<GetQueueDetailModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<GetQueueDetailModel> {
                return retrofitApi.getQueueDetail(casinoQueueModel?.id!!.toString())
            }

            override fun onResponse(res: Response<GetQueueDetailModel>) {

                if (res.body()?.status == 200){
                    Log.e("Responsesss",Gson().toJson(res.body()))
                    statusModel.position.set(res.body()?.detail?.queue_no)
                    res.body()?.detail?.state_id?.toString()?.let { setView(it) }
                    counter?.start()
                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context,"Something went wrong.")
            }

        })

    }

    fun gotoHome(view:View){
        context.startActivity(Intent(context,FragmentLoadActivity::class.java))
        (context as Activity).finishAffinity()
    }



    private fun setView(state_id:String){
        when(state_id){
            "0"->{
                isWaiting.set(true)
                isAccepted.set(false)
                isRejected.set(false)
                isThanks.set(false)
            }
            "1"->{
                isWaiting.set(false)
                isAccepted.set(false)
                isRejected.set(false)
                isThanks.set(true)

            }
            "4"->{
                isWaiting.set(false)
                isAccepted.set(false)
                isRejected.set(true)
                isThanks.set(false)

            }
            "3"->{
                isWaiting.set(false)
                isAccepted.set(true)
                isRejected.set(false)
                isThanks.set(false)
            }
            else->
            {
                isWaiting.set(true)
                isAccepted.set(false)
                isRejected.set(false)
                isThanks.set(false)

            }
        }
    }

    data class StatusModel(
        var position: ObservableField<String> = ObservableField(""),
        var total_user: ObservableField<String> = ObservableField(""),
        var restName: ObservableField<String> = ObservableField(""),
        var banner: ObservableField<String> = ObservableField(""),
        var message: ObservableField<String> = ObservableField(""),
        var logo: ObservableField<String> = ObservableField("")
    )
}