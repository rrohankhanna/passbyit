package com.qyou.android.views

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.lifecycle.ViewModel
import com.qyou.android.R
import com.qyou.android.utils.AppClass
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.PreferenceFile
import com.qyou.android.utils.UpdateLanguageModel
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.WebClientSetup.WebUrls
import com.qyou.android.utils.models.EnterQueueModel
import com.qyou.android.utils.models.GetCasinoDetailModel
import com.qyou.android.views.pendingrequest.PendingRequest
import kotlinx.android.synthetic.main.choose_language.view.*
import kotlinx.android.synthetic.main.confirm_queue_alert.view.*
import retrofit2.Response

@SuppressLint("NotifyDataSetChanged")
class MainActivityVM(var context: Context, val mainActivity: MainActivity):ViewModel() {

    var drawables:ArrayList<Int> = ArrayList()
    var mainAdapter = MainAdapter(context,drawables)

    init {
        drawables.add(R.drawable.casino)
        drawables.add(R.color.grey)
        drawables.add(R.color.grey)
        drawables.add(R.color.grey)
        mainAdapter.notifyDataSetChanged()

    }



    fun onCLickScanner(view:View){
        context.startActivity(Intent(context,Scanner::class.java))
    }

    fun onClickViewTickets(view: View){
        context.startActivity(Intent(context,PendingRequest::class.java))
    }


    fun onClickLanguage(view: View){
        var customDialog: androidx.appcompat.app.AlertDialog? = null
        val customAlertBuilder = androidx.appcompat.app.AlertDialog.Builder(context)

        val customAlertView =
            LayoutInflater.from(context).inflate(R.layout.choose_language, null)
        customAlertBuilder.setView(customAlertView)
        //alertDialogBuilder.setTitle(context.getString(R.string.app_name))
        customDialog = customAlertBuilder.create()
        customDialog?.show()

        customDialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        customDialog?.setCancelable(false)
        customDialog?.setCanceledOnTouchOutside(false)

        customAlertView.tvEnglish.setOnClickListener {
            PreferenceFile.storeKey(context,WebUrls.LANGUAGE_KEY,"en")
            updateLanguage("en")
            //mainActivity.changeLocale("en")
            customDialog.dismiss()
        }
        customAlertView.tvFrench.setOnClickListener {
            PreferenceFile.storeKey(context,WebUrls.LANGUAGE_KEY,"fr")
            updateLanguage("fr")
            //mainActivity.changeLocale("fr")
            customDialog.dismiss()
        }
        customAlertView.tvCancel.setOnClickListener {
            customDialog.dismiss()
        }
    }

    private fun updateLanguage(lang:String){
        RetrofitClientBuilder.callService(context,true,"",object :
            RequestProcess<Response<UpdateLanguageModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<UpdateLanguageModel> {
                return retrofitApi.changeLanguage(lang,AppClass.token)
            }

            override fun onResponse(res: Response<UpdateLanguageModel>) {
                //mainActivity.recreate()
                context.startActivity(Intent(context,FragmentLoadActivity::class.java))
                (context as Activity).finishAffinity()
                if (res.body()?.status == 200){
                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context,message)
            }

        })


    }


    fun getRestaurantDetail(restId:String,bookingId:String,title:String,message:String){
        RetrofitClientBuilder.callService(context,true,"",object :
            RequestProcess<Response<GetCasinoDetailModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<GetCasinoDetailModel> {
                return retrofitApi.getRestaurantDetail(restId,AppClass.token)
            }

            override fun onResponse(res: Response<GetCasinoDetailModel>) {

                if (res.body()?.status == 200){
                    val bundle = Bundle()
                    if (res.body()?.detail?.type_id == 3 || res.body()?.detail?.type_id == 5){
                        val queueModel = EnterQueueModel.Detail()
                        queueModel?.image = res.body()?.detail?.profile_file
                        queueModel?.logo = res.body()?.detail?.logo_file
                        queueModel?.name = res.body()?.detail?.full_name
                        queueModel?.queue_no = ""
                        queueModel?.pending_users = ""
                        queueModel.id = bookingId.toInt()
                        queueModel?.total_registered_users = res.body()?.detail?.total_registered_users
                        queueModel?.type = context.getString(R.string.people)
                        queueModel.title = title
                        queueModel.message = message
                        queueModel.fromNotification = true
                        bundle.putSerializable("queue_model",queueModel)
                        context.startActivity(Intent(context, QueueNumberActivity::class.java).also { it.putExtras(bundle) })

/*
                        startActivity(Intent(this@Scanner, RestaurantDetailActivity::class.java).putExtras(bundle))
                        finish()
*/
                    }
                    else if (res.body()?.detail?.type_id == 2){
                        val queueModel = EnterQueueModel.Detail()
                        queueModel.image = res.body()?.detail?.profile_file
                        queueModel.logo = res.body()?.detail?.logo_file
                        queueModel.name = res.body()?.detail?.full_name
                        queueModel.id = bookingId.toInt()
                        queueModel.type = context.getString(R.string.groups)
                        queueModel.queue_no = ""
                        queueModel.pending_users = ""
                        queueModel.groupNumber = ""
                        queueModel.title = title
                        queueModel.message = message
                        queueModel.fromNotification = true
                        queueModel.total_registered_users = res.body()?.detail?.total_registered_users
                        bundle.putSerializable("queue_model",queueModel)
                        context.startActivity(Intent(context,QueueNumberActivity::class.java).also { it.putExtras(bundle) })

/*
                        startActivity(Intent(this@Scanner, GroupNumberActivity::class.java).putExtras(bundle))
                        finish()
*/
                    }

                    (context as Activity).finish()
                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context,message)
            }

        })

    }


}