package com.qyou.android.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.qyou.android.R
import com.qyou.android.databinding.ActivityRestaurantDetailBinding

class RestaurantDetailActivity : AppCompatActivity() {
    private lateinit var restaurantDetailVM: RestaurantDetailVM
    private var activityRestaurantDetailBinding: ActivityRestaurantDetailBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityRestaurantDetailBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_restaurant_detail)
        restaurantDetailVM = RestaurantDetailVM(this,this)
        activityRestaurantDetailBinding!!.restaurantDetailVM = restaurantDetailVM
        activityRestaurantDetailBinding!!.restaurantDetailModel = restaurantDetailVM.restaurantDetail
    }
}