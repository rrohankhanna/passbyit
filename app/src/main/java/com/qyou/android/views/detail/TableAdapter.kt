package com.qyou.android.views.detail

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.qyou.android.R
import com.qyou.android.databinding.TableAdapterLayoutBinding
import com.qyou.android.utils.models.GetCasinoDetailModel
import com.qyou.android.utils.models.RestaurantDetailModel

class TableAdapter(
    var context: Context, val tableList:ArrayList<GetCasinoDetailModel.Detail.ActiveTable>?
) : RecyclerView.Adapter<TableAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val tableAdapterLayoutBinding:TableAdapterLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.table_adapter_layout,
            parent,
            false
        )
        return ViewHolder(tableAdapterLayoutBinding)
    }

    override fun getItemCount(): Int {
        return tableList?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.setIsRecyclable(false)
        holder.setData(tableList!![position])
        holder.itemView.animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
    }

    inner class ViewHolder(var tableAdapterLayoutBinding:TableAdapterLayoutBinding) :
        RecyclerView.ViewHolder(tableAdapterLayoutBinding.root) {


        fun setData(activeTable: GetCasinoDetailModel.Detail.ActiveTable) {
            val tableAdapterVM = TableAdapterVM(context,activeTable)
            tableAdapterLayoutBinding.tableAdapterVM = tableAdapterVM
            tableAdapterLayoutBinding.executePendingBindings()
        }
    }
}
