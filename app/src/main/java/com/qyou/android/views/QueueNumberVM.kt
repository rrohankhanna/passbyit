package com.qyou.android.views

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.qyou.android.R
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder.customDialog
import com.qyou.android.utils.models.*
import kotlinx.android.synthetic.main.cancel_alert.view.*
import kotlinx.android.synthetic.main.confirm_queue_alert.view.*
import retrofit2.Response

class QueueNumberVM(var context: Context, var queueNumberActivity: QueueNumberActivity) :
    ViewModel() {

    var enterQueueModel: EnterQueueModel.Detail? = null
    var queueModel = QueueModel()
    var counter: CountDownTimer? = null
    var isThanks = ObservableBoolean(false)
    var your_turn = ObservableBoolean(false)
    var isGroup = ObservableBoolean(false)


    init {
        getBundleData()
    }

    private fun getBundleData() {
        enterQueueModel = queueNumberActivity.intent.extras!!.getSerializable("queue_model") as EnterQueueModel.Detail
        isGroup.set(enterQueueModel?.type != context.getString(R.string.people))
        queueModel.restName.set(enterQueueModel?.name)
        queueModel.groupNumber.set(enterQueueModel?.groupNumber)
        queueModel.type.set(enterQueueModel?.type)
        queueModel.banner.set(enterQueueModel?.logo)
        queueModel.total_user.set(enterQueueModel?.total_registered_users)
        queueModel.position.set(enterQueueModel?.queue_no)
        queueModel.logo.set(enterQueueModel?.image)
        queueModel.people_ahead.set(enterQueueModel?.pending_users)
        your_turn.set(enterQueueModel?.state_id ==5)
        if (enterQueueModel!!.fromNotification){
            confirmAlert()
        }

        startTimeCounter()
    }

    fun onClickCancel(view: View){
        var customDialog: androidx.appcompat.app.AlertDialog? = null
        val customAlertBuilder = androidx.appcompat.app.AlertDialog.Builder(context)

        val customAlertView =
            LayoutInflater.from(context).inflate(R.layout.cancel_alert, null)
        customAlertBuilder.setView(customAlertView)
        //alertDialogBuilder.setTitle(context.getString(R.string.app_name))
        customDialog = customAlertBuilder.create()
        customDialog?.show()

        customDialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        customDialog?.setCancelable(true)
        customDialog?.setCanceledOnTouchOutside(true)

        customAlertView.btnCancel.setOnClickListener {
            if (customAlertView.et_reason.text.trim().isNotEmpty()) {
                customDialog?.dismiss()
                cancelBookingApi(customAlertView.et_reason.text.trim().toString())
            } else {
                CommonMethods.showToast(context, context.getString(R.string.please_enter_reason_for_cancellation))
            }
        }
    }


    private fun confirmAlert(){
        var customDialog: androidx.appcompat.app.AlertDialog? = null
        val customAlertBuilder = androidx.appcompat.app.AlertDialog.Builder(context)

        val customAlertView =
            LayoutInflater.from(context).inflate(R.layout.confirm_queue_alert, null)
        customAlertBuilder.setView(customAlertView)
        //alertDialogBuilder.setTitle(context.getString(R.string.app_name))
        customDialog = customAlertBuilder.create()
        customDialog?.show()

        customDialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        customDialog?.setCancelable(false)
        customDialog?.setCanceledOnTouchOutside(false)
        customAlertView.tvTitle.text = enterQueueModel!!.title!!
        customAlertView.tvMessage.text = enterQueueModel!!.message!!

        customAlertView.tvNo.setOnClickListener {
            customDialog?.dismiss()
        }
        customAlertView.tvYes.setOnClickListener {
            customDialog.dismiss()
            hitConfirmQueueApi()
        }
    }


    private fun hitConfirmQueueApi(){
        RetrofitClientBuilder.callService(context, true, "", object :
            RequestProcess<Response<ConfirmQueueModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<ConfirmQueueModel> {
                return retrofitApi.confirmQueueModel(enterQueueModel?.id!!.toString())
            }

            override fun onResponse(res: Response<ConfirmQueueModel>) {

                if (res.body()?.status == 200) {
                    context.startActivity(Intent(context, FragmentLoadActivity::class.java))
                    (context as Activity).finishAffinity()
                }
                else{
                    confirmAlert()                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context, "Something went wrong.")
            }

        })

    }




    private fun cancelBookingApi(reason:String) {
        RetrofitClientBuilder.callService(context, true, "", object :
            RequestProcess<Response<CancelBookingModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<CancelBookingModel> {
                return retrofitApi.cancelBooking(enterQueueModel?.id!!.toString(),reason)
            }

            override fun onResponse(res: Response<CancelBookingModel>) {

                if (res.body()?.status == 200) {
                    CommonMethods.showToast(context,res.body()?.message!!)
                    context.startActivity(Intent(context, FragmentLoadActivity::class.java))
                    (context as Activity).finishAffinity()

                }
                else{
                    CommonMethods.showToast(context,res.body()?.error!!)
                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context, "Something went wrong.")
            }

        })
    }


    private fun startTimeCounter() {
        counter = object : CountDownTimer(3000, 300) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                Log.e("sdsdfsdf", "sjdfsbjf")
                getQueueDetail()
            }
        }.start()
    }


    private fun getQueueDetail() {
        RetrofitClientBuilder.callService(context, false, "", object :
            RequestProcess<Response<GetQueueDetailModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<GetQueueDetailModel> {
                return retrofitApi.getQueueDetail(enterQueueModel?.id!!.toString())
            }

            override fun onResponse(res: Response<GetQueueDetailModel>) {

                if (res.body()?.status == 200) {
                    Log.e("Responsesss", Gson().toJson(res.body()))
                    queueModel.position.set(res.body()?.detail?.queue_no)
                    queueModel.people_ahead.set(res.body()?.detail?.pending_users)
                    isThanks.set(res.body()?.detail?.state_id == 1)
                    your_turn.set(res.body()?.detail?.state_id ==5)
                    if (isGroup.get()) {
                        queueModel.groupNumber.set(
                            when (res.body()?.detail?.group_person) {
                                "1" -> "1-2"
                                "2" -> "3-4"
                                "3" -> "5-6"
                                "4" -> "7-8"
                                else -> "1-2"
                            }
                        )
                    }
                    counter?.start()
                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context, "Something went wrong.")
            }

        })

    }


    fun gotoHome(view: View) {
        context.startActivity(Intent(context, FragmentLoadActivity::class.java))
        (context as Activity).finishAffinity()

    }


    data class QueueModel(
        var position: ObservableField<String> = ObservableField(""),
        var people_ahead: ObservableField<String> = ObservableField(""),
        var groupNumber: ObservableField<String> = ObservableField(""),
        var type: ObservableField<String> = ObservableField(""),
        var total_user: ObservableField<String> = ObservableField(""),
        var restName: ObservableField<String> = ObservableField(""),
        var banner: ObservableField<String> = ObservableField(""),
        var message: ObservableField<String> = ObservableField(""),
        var logo: ObservableField<String> = ObservableField("")
    )
}