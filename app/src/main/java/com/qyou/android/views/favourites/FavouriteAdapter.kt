package com.qyou.android.views.favourites

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.qyou.android.R
import com.qyou.android.databinding.AdapterFavouriteLayoutBinding
import com.qyou.android.utils.models.FavouriteListModel

class FavouriteAdapter(
    var context: Context,
    val list: ArrayList<FavouriteListModel.FavouriteList>
) : RecyclerView.Adapter<FavouriteAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val adapterFavouritesBinding:AdapterFavouriteLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.adapter_favourite_layout,
            parent,
            false
        )
        return ViewHolder(adapterFavouritesBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.setIsRecyclable(false)
        holder.setData(list[position])
        holder.itemView.animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)

    }

    inner class ViewHolder(var adapterFavouritesBinding:AdapterFavouriteLayoutBinding) :
        RecyclerView.ViewHolder(adapterFavouritesBinding.root) {


        fun setData(model: FavouriteListModel.FavouriteList) {
            val favouriteAdapterVM = FavouriteAdapterVM(context,model)
            adapterFavouritesBinding.favouriteAdapterVM = favouriteAdapterVM
            adapterFavouritesBinding.executePendingBindings()
        }
    }
}
