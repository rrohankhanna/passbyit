package com.qyou.android.views

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.qyou.android.R
import com.qyou.android.databinding.MainAdapterLayoutBinding
import com.qyou.android.views.restaurantList.RestaurantList

class MainAdapter(
    var context: Context,
    val drawables: ArrayList<Int>
) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mainAdapterBinding: MainAdapterLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.main_adapter_layout,
            parent,
            false
        )
        return ViewHolder(mainAdapterBinding)
    }

    override fun getItemCount(): Int {
        return drawables.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.setIsRecyclable(false)
        holder.setData(drawables[position])
        holder.itemView.animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)

        holder.rivRestImg.setImageResource(drawables[position])

        holder.itemView.setOnClickListener {
            if (position == 0){
                context.startActivity(Intent(context, RestaurantList::class.java))
            }
        }
    }

    inner class ViewHolder(var mainAdapterBinding: MainAdapterLayoutBinding) :
        RecyclerView.ViewHolder(mainAdapterBinding.root) {

        var rivRestImg = mainAdapterBinding.rivRestImg

        fun setData(i: Int) {
            val mainAdapterVM = MainAdapterVM(context)
            mainAdapterBinding.mainAdapterVM = mainAdapterVM
            mainAdapterBinding.executePendingBindings()
        }
    }
}
