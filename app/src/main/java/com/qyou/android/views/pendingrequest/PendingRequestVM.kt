package com.qyou.android.views.pendingrequest

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.qyou.android.R
import com.qyou.android.utils.AppClass
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.models.GetQueueDetailModel
import com.qyou.android.utils.models.PendingRequestModel
import retrofit2.Response

class PendingRequestVM(var context: Context , var pendingRequest: PendingRequest):ViewModel() {

    var list :ArrayList<PendingRequestModel.PendingList> = ArrayList()

    var pendingRequestAdapter = PendingRequestAdapter(context,list)

    init {
        getPendingList()
    }

    private fun getPendingList() {
        RetrofitClientBuilder.callService(context,true,"",object :
            RequestProcess<Response<PendingRequestModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<PendingRequestModel> {
                return retrofitApi.getPendingRequest(AppClass.token)
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(res: Response<PendingRequestModel>) {

                if (res.body()?.status == 200){
                    list.addAll(res.body()?.list!!)
                    pendingRequestAdapter.notifyDataSetChanged()
                }
                else{
                    CommonMethods.showToast(context,context.getString(R.string.no_pending_request))
                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context,"Something went wrong.")
            }

        })
    }
}