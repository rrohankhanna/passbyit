package com.qyou.android.views.detail

import android.content.Context
import android.view.View
import androidx.lifecycle.ViewModel
import com.qyou.android.utils.models.GetCasinoDetailModel
import com.qyou.android.utils.models.RestaurantDetailModel

class TableAdapterVM(
    var context: Context,
    val activeTable: GetCasinoDetailModel.Detail.ActiveTable
):ViewModel() {

    fun onSelectTable(view:View){
        DetailActivity.detailActivityVM?.selectListener?.onSelectItem("table",activeTable?.id.toString())
    }
}