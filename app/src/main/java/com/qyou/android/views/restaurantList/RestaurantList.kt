package com.qyou.android.views.restaurantList

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.qyou.android.R
import com.qyou.android.databinding.CasinoListBinding

class RestaurantList : AppCompatActivity() {

    var casinoListBinding:CasinoListBinding?=null
    var restaurantListVM:RestaurantListVM?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        casinoListBinding = DataBindingUtil.setContentView(this,R.layout.casino_list)
        restaurantListVM = RestaurantListVM(this)
        casinoListBinding!!.restaurantListVM = restaurantListVM
    }
}