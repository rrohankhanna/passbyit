package com.qyou.android.views

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.qyou.android.R
import com.qyou.android.utils.AppClass
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.models.AddFavouriteModel
import com.qyou.android.utils.models.EnterQueueModel
import com.qyou.android.utils.models.GetCasinoDetailModel
import retrofit2.Response

class GroupNumberVM(var context: Context , var groupNumberActivity: GroupNumberActivity):ViewModel() {

    var restaurantDetailModel : GetCasinoDetailModel.Detail?=null
    var restaurantDetail = RestaurantDetail()
    var groupId = ""
    var firstGroup = ObservableBoolean(false)
    var secondGroup = ObservableBoolean(false)
    var thirdGroup = ObservableBoolean(false)
    var fourthGroup = ObservableBoolean(false)

    init {
        getBundleData()
    }


    @SuppressLint("NotifyDataSetChanged")
    private fun getBundleData() {
        restaurantDetailModel = groupNumberActivity.intent.extras!!.getSerializable("restaurant_detail") as GetCasinoDetailModel.Detail
        restaurantDetail.availableSeat.set(restaurantDetailModel?.total_registered_users.toString())
        restaurantDetail.restName.set(restaurantDetailModel?.full_name)
        restaurantDetail.message.set(restaurantDetailModel?.qr_scan_message)
        //restaurantDetail.email.set(restaurantDetailModel?.email)
        restaurantDetail.banner.set(restaurantDetailModel?.profile_file)
        restaurantDetail.logo.set(restaurantDetailModel?.logo_file)
        restaurantDetailModel!!.is_favourite.let {
            restaurantDetail.isFavourite.set(it)
        }
    }


    fun onClickFavourite(view: View){
        RetrofitClientBuilder.callService(context,true,"",object :RequestProcess<Response<AddFavouriteModel>>{
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<AddFavouriteModel> {
                return retrofitApi.addToFavourite(restaurantDetailModel?.id.toString(),AppClass.token)
            }

            override fun onResponse(res: Response<AddFavouriteModel>) {
                if (res.body()?.status == 200){
                    restaurantDetail.isFavourite.set(!restaurantDetail.isFavourite.get())
                    CommonMethods.showToast(context,res.body()?.message!!)

                } else
                    CommonMethods.showToast(context,"Something went wrong.")
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context,message)
            }


        })
    }



    fun onClickGroup(id:String){
        groupId = id
        when(id){
            "1"->{
                firstGroup.set(true)
                secondGroup.set(false)
                thirdGroup.set(false)
                fourthGroup.set(false)
            }
            "2"->{
                firstGroup.set(false)
                secondGroup.set(true)
                thirdGroup.set(false)
                fourthGroup.set(false)

            }
            "3"->{
                firstGroup.set(false)
                secondGroup.set(false)
                thirdGroup.set(true)
                fourthGroup.set(false)

            }
            "4"->{
                firstGroup.set(false)
                secondGroup.set(false)
                thirdGroup.set(false)
                fourthGroup.set(true)

            }
        }
    }

    fun onClickConfirm(view: View){
        if (groupId.isNullOrEmpty()){
            Toast.makeText(context,context.getString(R.string.please_select_group),Toast.LENGTH_LONG).show()
        }
        else
        {
            RetrofitClientBuilder.callService(context,true,"",object :
                RequestProcess<Response<EnterQueueModel>> {
                override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<EnterQueueModel> {
                    return retrofitApi.startGroupQueue(restaurantDetailModel?.id!!.toString(),groupId,
                        AppClass.token,"1")
                }

                override fun onResponse(res: Response<EnterQueueModel>) {
                    if (res.body()?.status == 200){
                        val queueModel = res.body()?.detail
                        queueModel?.image = restaurantDetailModel?.profile_file
                        queueModel?.logo = restaurantDetailModel?.logo_file
                        queueModel?.name = restaurantDetailModel?.full_name
                        queueModel?.type = context.getString(R.string.groups)
                        queueModel?.groupNumber = when(groupId){
                            "1"->"1-2"
                            "2"->"3-4"
                            "3"->"5-6"
                            "4"->"7-8"
                            else -> "1-2"
                        }
                        val bundle = Bundle()
                        bundle.putSerializable("queue_model",queueModel)
                        context.startActivity(Intent(context,QueueNumberActivity::class.java).also { it.putExtras(bundle) })
                    }
                }

                override fun onException(message: String) {
                    CommonMethods.showToast(context,"Something went wrong.")
                }

            })

        }

    }











    data class RestaurantDetail(
        var availableSeat: ObservableField<String> = ObservableField(""),
        var phone: ObservableField<String> = ObservableField(""),
        var restName: ObservableField<String> = ObservableField(""),
        var name: ObservableField<String> = ObservableField(""),
        var email: ObservableField<String> = ObservableField(""),
        var banner: ObservableField<String> = ObservableField(""),
        var message: ObservableField<String> = ObservableField(""),
        var logo: ObservableField<String> = ObservableField(""),
        var isFavourite:ObservableBoolean = ObservableBoolean(false)

    )
}