package com.qyou.android.views.detail

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TimePicker
import androidx.databinding.DataBindingUtil
import com.qyou.android.R
import com.qyou.android.databinding.ActivityDetailBinding
import com.qyou.android.utils.CommonMethods

class DetailActivity : AppCompatActivity() {

    companion object{
        @SuppressLint("StaticFieldLeak")
        var detailActivityVM:DetailActivityVM?=null
    }
    var detailActivityBinding:ActivityDetailBinding?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        detailActivityBinding = DataBindingUtil.setContentView(this,R.layout.activity_detail)
        detailActivityVM = DetailActivityVM(this,this)
        detailActivityBinding!!.detailActivityVM = detailActivityVM
        detailActivityBinding!!.restaurantDetailVM = detailActivityVM!!.restaurantDetail
    }

     val timePickerDialogListener: TimePickerDialog.OnTimeSetListener =
        object : TimePickerDialog.OnTimeSetListener {
            override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {

                // logic to properly handle
                // the picked timings by user
                val formattedTime: String = when {
                    hourOfDay == 0 -> {
                        if (minute < 10) {
                            "${hourOfDay + 12}:0${minute} am"
                        } else {
                            "${hourOfDay + 12}:${minute} am"
                        }
                    }
                    hourOfDay > 12 -> {
                        if (minute < 10) {
                            "${hourOfDay - 12}:0${minute} pm"
                        } else {
                            "${hourOfDay - 12}:${minute} pm"
                        }
                    }
                    hourOfDay == 12 -> {
                        if (minute < 10) {
                            "${hourOfDay}:0${minute} pm"
                        } else {
                            "${hourOfDay}:${minute} pm"
                        }
                    }
                    else -> {
                        if (minute < 10) {
                            "${hourOfDay}:0${minute} am"
                        } else {
                            "${hourOfDay}:${minute} am"
                        }
                    }
                }
                detailActivityVM!!.restaurantDetail.arrivalTime.set(CommonMethods.convertTimeTo24Hr(formattedTime))
//                previewSelectedTimeTextView.text = formattedTime
            }
        }

}