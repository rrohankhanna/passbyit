package com.qyou.android.views.pendingrequest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.qyou.android.R
import com.qyou.android.databinding.ActivityMainBinding
import com.qyou.android.databinding.ActivityPendingRequestBinding

class PendingRequest : Fragment() {
    private lateinit var pendingRequestVM: PendingRequestVM
    private lateinit var activityPendingRequestBinding: ActivityPendingRequestBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        activityPendingRequestBinding = ActivityPendingRequestBinding.inflate(inflater)
        return activityPendingRequestBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val pendingRequestFactory = PendingRequestFactory(requireContext(),this)
        pendingRequestVM =
            ViewModelProviders.of(this, pendingRequestFactory).get(PendingRequestVM::class.java)
        activityPendingRequestBinding!!.pendingRequestVM = pendingRequestVM
    }

}