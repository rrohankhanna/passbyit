package com.qyou.android.views

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.qyou.android.R
import com.qyou.android.databinding.ActivityFragmentLoadBinding
import com.qyou.android.utils.PreferenceFile
import com.qyou.android.utils.WebClientSetup.WebUrls
import java.util.*

class FragmentLoadActivity : AppCompatActivity() {

    private lateinit var fragLoadVM: FragLoadVM
    private lateinit var activityFragmentLoadBinding: ActivityFragmentLoadBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (PreferenceFile.retrieveKey(this,WebUrls.LANGUAGE_KEY).isNullOrEmpty()|| PreferenceFile.retrieveKey(this, WebUrls.LANGUAGE_KEY) == "en") changeLocale("en") else changeLocale("fr")
        activityFragmentLoadBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_fragment_load)
        fragLoadVM = FragLoadVM(this)
        activityFragmentLoadBinding.fragLoadVM = fragLoadVM
        NavigationUI.setupWithNavController(activityFragmentLoadBinding.bottomNav,findNavController(R.id.nav_host))
        getBundleData()

    }

    private fun getBundleData() {
        if (this.intent.extras!=null){
            val bundle = intent.extras
            if (bundle!!.getString("type_id")== "11"){
                MainActivity.mainActivityVM!!.getRestaurantDetail(bundle.getString("created_by_id")!!,bundle.getString("model_id")!!,bundle.getString("title")!!,bundle.getString("body")!!)

            }

        }
    }

    fun changeLocale(lang:String){
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(
            config,
            baseContext.resources.displayMetrics
        )
    }
}