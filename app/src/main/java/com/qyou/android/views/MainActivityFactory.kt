package com.qyou.android.views

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class MainActivityFactory(
    val context: Context,
    val mainActivity: MainActivity
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(MainActivityVM::class.java)){
            return MainActivityVM(
                context,mainActivity
            ) as T
        }

        throw IllegalArgumentException("")
    }
}