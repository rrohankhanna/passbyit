package com.qyou.android.views.notifications

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.messaging.RemoteMessage
import com.qyou.android.R
import com.qyou.android.databinding.AdapterNotificationLayoutBinding
import com.qyou.android.utils.models.FavouriteListModel
import com.qyou.android.utils.models.NotificationListModel

class NotificationAdapter(
    var context: Context,
    val list: ArrayList<NotificationListModel.NotificationList>
) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val adapterNotificationLayoutBinding:AdapterNotificationLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.adapter_notification_layout,
            parent,
            false
        )
        return ViewHolder(adapterNotificationLayoutBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.setIsRecyclable(false)
        holder.setData(list[position])
        holder.itemView.animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)

    }

    inner class ViewHolder(var adapterNotificationLayoutBinding:AdapterNotificationLayoutBinding) :
        RecyclerView.ViewHolder(adapterNotificationLayoutBinding.root) {


        fun setData(model: NotificationListModel.NotificationList) {
            val notificationAdapterVM = NotificationAdapterVM(context,model)
            adapterNotificationLayoutBinding.notificationAdapterVM = notificationAdapterVM
            adapterNotificationLayoutBinding.executePendingBindings()
        }
    }
}
