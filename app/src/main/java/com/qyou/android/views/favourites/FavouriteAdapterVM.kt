package com.qyou.android.views.favourites

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModel
import com.qyou.android.R
import com.qyou.android.utils.AppClass
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.models.FavouriteListModel
import com.qyou.android.utils.models.GetCasinoDetailModel
import com.qyou.android.views.GroupNumberActivity
import com.qyou.android.views.RestaurantDetailActivity
import com.qyou.android.views.detail.DetailActivity
import retrofit2.Response

class FavouriteAdapterVM(var context: Context, val model: FavouriteListModel.FavouriteList) :
    ViewModel() {

    fun onClickItems(view: View) {
        RetrofitClientBuilder.callService(context, true, "", object :
            RequestProcess<Response<GetCasinoDetailModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<GetCasinoDetailModel> {
                return retrofitApi.getRestaurantDetail(
                    model.restaurant.id.toString(),
                    AppClass.token
                )
            }

            override fun onResponse(res: Response<GetCasinoDetailModel>) {

                if (res.body()?.status == 200) {

                    if (res.body()?.detail?.is_queue_started == "0") {
                        CommonMethods.openAlert(
                            context,
                            context.getString(R.string.registeration_not_open)
                        )
                    } else {
                        val bundle = Bundle()
                        bundle.putSerializable("restaurant_detail", res.body()?.detail)
                        if (res.body()?.detail?.type_id == 6) {
                            context.startActivity(
                                Intent(
                                    context,
                                    DetailActivity::class.java
                                ).putExtras(bundle)
                            )
                        } else if (res.body()?.detail?.type_id == 3 || res.body()?.detail?.type_id == 5) {
                            context.startActivity(
                                Intent(
                                    context,
                                    RestaurantDetailActivity::class.java
                                ).putExtras(bundle)
                            )
                        } else if (res.body()?.detail?.type_id == 2) {
                            context.startActivity(
                                Intent(
                                    context,
                                    GroupNumberActivity::class.java
                                ).putExtras(bundle)
                            )
                        }
                    }


                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context, message)
            }

        })
    }
}