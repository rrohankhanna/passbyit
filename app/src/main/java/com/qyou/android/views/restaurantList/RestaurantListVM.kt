package com.qyou.android.views.restaurantList

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.View
import androidx.lifecycle.ViewModel
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.models.RestaurantDetailModel
import com.qyou.android.utils.models.RestaurantListModel
import retrofit2.Response

class RestaurantListVM(var context: Context):ViewModel() {


    var restList:ArrayList<RestaurantDetailModel.Detail> = ArrayList()
    var restaurantListAdapter = RestaurantListAdapter(context,restList)


    init {
        getRestaurantList()
    }

    private fun getRestaurantList() {
        RetrofitClientBuilder.callService(context,true,"",object :RequestProcess<Response<RestaurantListModel>>{
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<RestaurantListModel> {

                val hashMap:HashMap<String,Any> = HashMap()
                hashMap["latitude"] = "30.7046"
                hashMap["longitude"] = "76.7179"
                hashMap["page"] = "0"
                hashMap["restaurant_type"] = "6"

                return retrofitApi.getRestaurantList(hashMap)
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(res: Response<RestaurantListModel>) {

                if (res.isSuccessful && res.body()?.status == 200){
                    restList.addAll(res.body()?.list!!)
                    restaurantListAdapter.notifyDataSetChanged()
                }
                else{
                    CommonMethods.showToast(context,"Something went wrong...")
                }

            }

            override fun onException(message: String) {
                CommonMethods.showToast(context,message)
            }

        })
    }

    fun onClickBack(view:View){
        (context as Activity).onBackPressed()
    }
}