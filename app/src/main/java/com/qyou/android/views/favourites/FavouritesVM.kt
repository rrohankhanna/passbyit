package com.qyou.android.views.favourites

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.ViewModel
import com.qyou.android.R
import com.qyou.android.utils.AppClass
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.models.FavouriteListModel
import com.qyou.android.utils.models.PendingRequestModel
import com.qyou.android.views.pendingrequest.PendingRequestAdapter
import retrofit2.Response

class FavouritesVM(var context: Context , var favouritesFragment: FavouritesFragment):ViewModel() {

    var list :ArrayList<FavouriteListModel.FavouriteList> = ArrayList()

    var favouriteAdapter = FavouriteAdapter(context,list)

    init {
        getFavouriteList()
    }


    private fun getFavouriteList() {
        RetrofitClientBuilder.callService(context,true,"",object :
            RequestProcess<Response<FavouriteListModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<FavouriteListModel> {
                return retrofitApi.getFavouriteList(AppClass.token,"0")
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(res: Response<FavouriteListModel>) {

                if (res.body()?.status == 200){
                    list.addAll(res.body()?.list!!)
                    favouriteAdapter.notifyDataSetChanged()
                }
                else{
                    CommonMethods.showToast(context,context.getString(R.string.no_data_found))
                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context,"Something went wrong.")
            }

        })
    }

}