package com.qyou.android.views.notifications

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.ViewModel
import com.qyou.android.R
import com.qyou.android.utils.AppClass
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.models.FavouriteListModel
import com.qyou.android.utils.models.NotificationListModel
import com.qyou.android.views.favourites.FavouriteAdapter
import retrofit2.Response

class NotificationVM(var context: Context , var notificationFragment: NotificationFragment):ViewModel() {

    var list :ArrayList<NotificationListModel.NotificationList> = ArrayList()

    var notificationAdapter = NotificationAdapter(context,list)

    init {
        getNotificationList()
    }


    private fun getNotificationList() {
        RetrofitClientBuilder.callService(context,true,"",object :
            RequestProcess<Response<NotificationListModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<NotificationListModel> {
                return retrofitApi.getNotificationList(AppClass.token,"0")
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(res: Response<NotificationListModel>) {

                if (res.body()?.status == 200){
                    list.addAll(res.body()?.list!!)
                    notificationAdapter.notifyDataSetChanged()
                }
                else{
                    CommonMethods.showToast(context,context.getString(R.string.no_data_found))
                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context,"Something went wrong.")
            }

        })
    }

}