package com.qyou.android.views.restaurantList

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.qyou.android.R
import com.qyou.android.databinding.CasinoListRecyclveiewDesignBinding
import com.qyou.android.utils.models.RestaurantDetailModel
import com.qyou.android.utils.models.RestaurantListModel

class RestaurantListAdapter(
    var context: Context,
    val list: ArrayList<RestaurantDetailModel.Detail>
) : RecyclerView.Adapter<RestaurantListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val casinoListRecyclveiewDesignBinding:CasinoListRecyclveiewDesignBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.casino_list_recyclveiew_design,
            parent,
            false
        )
        return ViewHolder(casinoListRecyclveiewDesignBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.setIsRecyclable(false)
        holder.setData(list[position])
        holder.itemView.animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)

    }

    inner class ViewHolder(var casinoListRecyclveiewDesignBinding:CasinoListRecyclveiewDesignBinding) :
        RecyclerView.ViewHolder(casinoListRecyclveiewDesignBinding.root) {


        fun setData(restClass: RestaurantDetailModel.Detail) {
            val restaurantListAdapterVM = RestaurantListAdapterVM(context,restClass)
            casinoListRecyclveiewDesignBinding.restrauntListAdapterVM = restaurantListAdapterVM
            casinoListRecyclveiewDesignBinding.executePendingBindings()
        }
    }
}
