package com.qyou.android.views.notifications

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.qyou.android.views.pendingrequest.PendingRequest
import com.qyou.android.views.pendingrequest.PendingRequestVM
import java.lang.IllegalArgumentException

class NotificationFactory(
    val context: Context,
    val notificationFragment: NotificationFragment
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(NotificationVM::class.java)){
            return NotificationVM(
                context,notificationFragment
            ) as T
        }

        throw IllegalArgumentException("")
    }
}