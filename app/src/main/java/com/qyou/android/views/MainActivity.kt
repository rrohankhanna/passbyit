package com.qyou.android.views

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.qyou.android.R
import com.qyou.android.databinding.ActivityMainBinding
import com.qyou.android.utils.PreferenceFile
import com.qyou.android.utils.WebClientSetup.WebUrls
import java.util.*

class MainActivity : Fragment() {
    companion object{
        var mainActivityVM:MainActivityVM?=null
    }
    var mainActivityBinding:ActivityMainBinding?=null
    private lateinit var bundle: Bundle




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mainActivityBinding = ActivityMainBinding.inflate(inflater)
        return mainActivityBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivityFactory = MainActivityFactory(requireContext(),this)
        mainActivityVM =
            ViewModelProviders.of(this, mainActivityFactory).get(MainActivityVM::class.java)
        mainActivityBinding!!.mainActivityVM = mainActivityVM
    }




}