package com.qyou.android.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.qyou.android.R
import com.qyou.android.databinding.ActivityQueueNumberBinding

class QueueNumberActivity : AppCompatActivity() {
    private lateinit var queueNumberVM: QueueNumberVM
    private lateinit var activityQueueNumberBinding: ActivityQueueNumberBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activityQueueNumberBinding = DataBindingUtil.setContentView(this,R.layout.activity_queue_number)
        queueNumberVM = QueueNumberVM(this,this)
        activityQueueNumberBinding!!.queueNumberVM = queueNumberVM
        activityQueueNumberBinding!!.queueModel = queueNumberVM!!.queueModel

    }

    override fun onBackPressed() {

    }

    override fun onStop() {
        super.onStop()
        if (queueNumberVM!!.counter!=null){
            queueNumberVM!!.counter!!.cancel()
        }
    }

    override fun onPause() {
        super.onPause()
        if (queueNumberVM!!.counter!=null){
            queueNumberVM!!.counter!!.cancel()
        }
    }

    override fun onResume() {
        super.onResume()
        if (queueNumberVM!!.counter!=null){
            queueNumberVM!!.counter!!.start()
        }
    }


}