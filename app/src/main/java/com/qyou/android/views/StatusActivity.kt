package com.qyou.android.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.qyou.android.R
import com.qyou.android.databinding.ActivityStatusBinding

class StatusActivity : AppCompatActivity() {

    var statusActivityBinding:ActivityStatusBinding?=null
    var statusActivityVM:StatusActivityVM?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        statusActivityBinding = DataBindingUtil.setContentView(this,R.layout.activity_status)
        statusActivityVM = StatusActivityVM(this,this)
        statusActivityBinding!!.statusActivityVM = statusActivityVM
        statusActivityBinding!!.statusModel = statusActivityVM!!.statusModel
    }


    override fun onBackPressed() {

    }

    override fun onStop() {
        super.onStop()
        if (statusActivityVM!!.counter!=null){
            statusActivityVM!!.counter!!.cancel()
        }
    }

    override fun onPause() {
        super.onPause()
        if (statusActivityVM!!.counter!=null){
            statusActivityVM!!.counter!!.cancel()
        }
    }

    override fun onResume() {
        super.onResume()
        if (statusActivityVM!!.counter!=null){
            statusActivityVM!!.counter!!.start()
        }
    }


}