package com.qyou.android.views

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ErrorCallback
import com.budiyev.android.codescanner.ScanMode
import com.qyou.android.R
import com.qyou.android.utils.AppClass
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.models.GetCasinoDetailModel
import com.qyou.android.utils.models.GetLongUrlModel
import com.qyou.android.views.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_scanner.*
import retrofit2.Response

class Scanner : AppCompatActivity() {

    var codeScanner: CodeScanner? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)

        codeScanner = CodeScanner(this, scannerView)
        codeScanner!!.camera = CodeScanner.CAMERA_BACK
        codeScanner!!.formats = CodeScanner.ALL_FORMATS
        codeScanner!!.autoFocusMode = AutoFocusMode.SAFE
        codeScanner!!.scanMode = ScanMode.SINGLE
        codeScanner!!.isAutoFocusEnabled = true
        codeScanner!!.isFlashEnabled = false
        scanResult()

    }


    private fun scanResult() {
        codeScanner!!.decodeCallback = DecodeCallback {
            runOnUiThread {
                it.text?.let {
                    hitGetLongUrlApi(it)
                }


//                if (it.text.contains("https://passby.to/"))
//                    getRestaurantDetail(it.text.replace("https://passby.to/business/detail/", ""))
//                else
//                    Toast.makeText(this, "Invalid Result!", Toast.LENGTH_LONG).show()
            }
        }

        codeScanner!!.errorCallback = ErrorCallback {
            runOnUiThread {
                //Toast.makeText(this, "Camera Error:${it.message}", Toast.LENGTH_LONG).show()
                onBackPressed()
            }
        }
        checkPermission()

    }

    private fun hitGetLongUrlApi(url: String) {

        RetrofitClientBuilder.callService(this,true,"",object :RequestProcess<Response<GetLongUrlModel>>{
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<GetLongUrlModel> {
                return retrofitApi.getLongUrl(url)
            }

            override fun onResponse(res: Response<GetLongUrlModel>) {

                if (res.body()?.status == 200){
                    res.body()?.returnUrl?.let {
                        if (it.contains("https"))
                            getRestaurantDetail(it.replace("https://passby.to/business/detail/", ""))
                        else if (it.contains("http"))
                            getRestaurantDetail(it.replace("http://passby.to/business/detail/", ""))


                    }

                }
                else CommonMethods.showToast(this@Scanner,res.body()?.error!!)
            }

            override fun onException(message: String) {
                CommonMethods.showToast(this@Scanner,message)
            }

        })



    }

    private fun checkPermission() {

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 100)
        } else {
            codeScanner!!.startPreview()
        }

    }

    override fun onResume() {
        super.onResume()

        codeScanner!!.startPreview()
    }

    override fun onPause() {
        codeScanner!!.releaseResources()
        super.onPause()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 100 && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            codeScanner!!.startPreview()
        } else {

            checkPermission()
        }
    }

    private fun getRestaurantDetail(id:String){
        RetrofitClientBuilder.callService(this,true,"",object :RequestProcess<Response<GetCasinoDetailModel>>{
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<GetCasinoDetailModel> {
                return retrofitApi.getRestaurantDetail(id,AppClass.token)
            }

            override fun onResponse(res: Response<GetCasinoDetailModel>) {

                if (res.body()?.status == 200){

                    if (res.body()?.detail?.is_queue_started == "0"){
                        CommonMethods.openAlert(this@Scanner,this@Scanner.getString(R.string.registeration_not_open))
                    }
                    else
                    {
                        val bundle = Bundle()
                        bundle.putSerializable("restaurant_detail",res.body()?.detail)
                        if (res.body()?.detail?.type_id == 6){
                            startActivity(Intent(this@Scanner,DetailActivity::class.java).putExtras(bundle))
                            finish()
                        }
                        else if (res.body()?.detail?.type_id == 3 || res.body()?.detail?.type_id == 5){
                            startActivity(Intent(this@Scanner,RestaurantDetailActivity::class.java).putExtras(bundle))
                            finish()
                        }
                        else if (res.body()?.detail?.type_id == 2){
                            startActivity(Intent(this@Scanner,GroupNumberActivity::class.java).putExtras(bundle))
                            finish()
                        }
                    }


                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(this@Scanner,message)
            }

        })
    }
}
