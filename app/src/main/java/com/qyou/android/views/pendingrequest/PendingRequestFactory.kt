package com.qyou.android.views.pendingrequest

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class PendingRequestFactory(
    val context: Context,
    val pendingRequest: PendingRequest
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(PendingRequestVM::class.java)){
            return PendingRequestVM(
                context,pendingRequest
            ) as T
        }

        throw IllegalArgumentException("")
    }
}