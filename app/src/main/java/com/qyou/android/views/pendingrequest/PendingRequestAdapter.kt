package com.qyou.android.views.pendingrequest

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.qyou.android.R
import com.qyou.android.databinding.AdapterPendingRequestBinding
import com.qyou.android.utils.models.PendingRequestModel

class PendingRequestAdapter(
    var context: Context,
    val list: ArrayList<PendingRequestModel.PendingList>
) : RecyclerView.Adapter<PendingRequestAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val adapterPendingRequestBinding:AdapterPendingRequestBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.adapter_pending_request,
            parent,
            false
        )
        return ViewHolder(adapterPendingRequestBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.setIsRecyclable(false)
        holder.setData(list[position])
        holder.itemView.animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)

    }

    inner class ViewHolder(var adapterPendingRequestBinding:AdapterPendingRequestBinding) :
        RecyclerView.ViewHolder(adapterPendingRequestBinding.root) {


        fun setData(model:PendingRequestModel.PendingList) {
            val pendingRequestAdapterVM = PendingrequestAdapterVM(context,model)
            adapterPendingRequestBinding.pendingRequestAdapterVM = pendingRequestAdapterVM
            adapterPendingRequestBinding.executePendingBindings()
        }
    }
}
