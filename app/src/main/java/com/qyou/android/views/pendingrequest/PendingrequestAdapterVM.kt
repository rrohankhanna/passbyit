package com.qyou.android.views.pendingrequest

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.qyou.android.R
import com.qyou.android.utils.AppClass
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.models.CasinoQueueModel
import com.qyou.android.utils.models.EnterQueueModel
import com.qyou.android.utils.models.GetCasinoDetailModel
import com.qyou.android.utils.models.PendingRequestModel
import com.qyou.android.views.GroupNumberActivity
import com.qyou.android.views.QueueNumberActivity
import com.qyou.android.views.RestaurantDetailActivity
import com.qyou.android.views.StatusActivity
import com.qyou.android.views.detail.DetailActivity
import retrofit2.Response

class PendingrequestAdapterVM(var context: Context, val model: PendingRequestModel.PendingList) {

    fun onClickItem(view:View){
        RetrofitClientBuilder.callService(context,true,"",object :
            RequestProcess<Response<GetCasinoDetailModel>> {
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<GetCasinoDetailModel> {
                return retrofitApi.getRestaurantDetail(model.restaurant_id.toString(),AppClass.token)
            }

            override fun onResponse(res: Response<GetCasinoDetailModel>) {

                if (res.body()?.status == 200){
                    val bundle = Bundle()
                    if (res.body()?.detail?.type_id == 6){
                        val casinoQueueModel = CasinoQueueModel.Detail()
                        casinoQueueModel.id = model.id
                        casinoQueueModel.message = res.body()?.detail?.qr_scan_message
                        casinoQueueModel.name = res.body()?.detail?.full_name
                        casinoQueueModel.image = res.body()?.detail?.profile_file
                        casinoQueueModel.logo = res.body()?.detail?.logo_file
                        casinoQueueModel.total_registered_users = res.body()?.detail?.total_registered_users
                        casinoQueueModel.queue_no = model.queue_no
                        bundle.putSerializable("casino_model",casinoQueueModel)
                        context.startActivity(Intent(context, StatusActivity::class.java).putExtras(bundle))
                    }
                    else if (res.body()?.detail?.type_id == 3 || res.body()?.detail?.type_id == 5){
                        val queueModel = EnterQueueModel.Detail()
                        queueModel?.image = res.body()?.detail?.profile_file
                        queueModel?.logo = res.body()?.detail?.logo_file
                        queueModel?.name = res.body()?.detail?.full_name
                        queueModel?.queue_no = model.queue_no
                        queueModel?.pending_users = model.pending_users
                        queueModel.id = model.id
                        queueModel?.total_registered_users = res.body()?.detail?.total_registered_users
                        queueModel?.type = context.getString(R.string.people)
                        bundle.putSerializable("queue_model",queueModel)
                        context.startActivity(Intent(context, QueueNumberActivity::class.java).also { it.putExtras(bundle) })

/*
                        startActivity(Intent(this@Scanner, RestaurantDetailActivity::class.java).putExtras(bundle))
                        finish()
*/
                    }
                    else if (res.body()?.detail?.type_id == 2){
                        val queueModel = EnterQueueModel.Detail()
                        queueModel.image = res.body()?.detail?.profile_file
                        queueModel.logo = res.body()?.detail?.logo_file
                        queueModel.name = res.body()?.detail?.full_name
                        queueModel.id = model.id
                        queueModel.type = context.getString(R.string.groups)
                        queueModel.queue_no = model.queue_no
                        queueModel.pending_users = model.pending_users
                        queueModel.groupNumber = when(model.group_person){
                            "1"->"1-2"
                            "2"->"3-4"
                            "3"->"5-6"
                            "4"->"7-8"
                            else -> "1-2"
                        }
                        queueModel.total_registered_users = res.body()?.detail?.total_registered_users
                        bundle.putSerializable("queue_model",queueModel)
                        context.startActivity(Intent(context,QueueNumberActivity::class.java).also { it.putExtras(bundle) })

/*
                        startActivity(Intent(this@Scanner, GroupNumberActivity::class.java).putExtras(bundle))
                        finish()
*/
                    }
                }
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context,message)
            }

        })

    }
}