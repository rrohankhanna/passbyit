package com.qyou.android.views.notifications

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.qyou.android.R
import com.qyou.android.databinding.FragmentNotificationBinding
import com.qyou.android.views.favourites.FavouritesVM


class NotificationFragment : Fragment() {

    private lateinit var notificationVM: NotificationVM
    private lateinit var fragmentNotificationBinding: FragmentNotificationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        fragmentNotificationBinding = FragmentNotificationBinding.inflate(inflater)
        return fragmentNotificationBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val notificationFactory = NotificationFactory(requireContext(),this)
        notificationVM =
            ViewModelProviders.of(this, notificationFactory).get(NotificationVM::class.java)
        fragmentNotificationBinding!!.notificationVM = notificationVM
    }


}