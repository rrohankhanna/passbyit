package com.qyou.android.views.detail

import android.annotation.SuppressLint
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.qyou.android.R
import com.qyou.android.utils.AppClass
import com.qyou.android.utils.CommonMethods
import com.qyou.android.utils.PreferenceFile
import com.qyou.android.utils.SelectListener
import com.qyou.android.utils.WebClientSetup.RequestProcess
import com.qyou.android.utils.WebClientSetup.RetrofitAPIs
import com.qyou.android.utils.WebClientSetup.RetrofitClientBuilder
import com.qyou.android.utils.WebClientSetup.WebUrls
import com.qyou.android.utils.models.*
import com.qyou.android.views.MainActivity
import com.qyou.android.views.StatusActivity
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class DetailActivityVM(var context: Context, val detailActivity: DetailActivity):ViewModel(),SelectListener {


    val tableList:ArrayList<GetCasinoDetailModel.Detail.ActiveTable> = ArrayList()
    var numberAdapter = NumberAdapter(context)
    var tableAdapter = TableAdapter(context,tableList)
    var restaurantDetailModel :GetCasinoDetailModel.Detail?=null
    var restaurantDetail = RestaurantDetail()
    var people = ""
    var selectedTableList :ArrayList<String> = ArrayList()
    var selectListener:SelectListener?=null
    var hashMap:HashMap<String,Any> = HashMap()
    val lang :String= if (PreferenceFile.retrieveKey(context,WebUrls.LANGUAGE_KEY).isNullOrEmpty()) "en" else PreferenceFile.retrieveKey(context,WebUrls.LANGUAGE_KEY)!!

    init {
        selectListener = this
        getBundleData()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getBundleData() {
        restaurantDetailModel = detailActivity.intent.extras!!.getSerializable("restaurant_detail") as GetCasinoDetailModel.Detail
        restaurantDetail.availableSeat.set(restaurantDetailModel?.total_registered_users.toString())
        restaurantDetail.restName.set(restaurantDetailModel?.full_name)
        restaurantDetail.message.set(restaurantDetailModel?.qr_scan_message)
        //restaurantDetail.email.set(restaurantDetailModel?.email)
        restaurantDetail.banner.set(restaurantDetailModel?.logo_file)
        restaurantDetail.logo.set(restaurantDetailModel?.profile_file)
        restaurantDetailModel!!.is_favourite.let {
            restaurantDetail.isFavourite.set(it)
        }
        if (restaurantDetailModel?.is_tables_opened!!){
            restaurantDetail.userText.set(context.getString(R.string.total_player_wait))
            restaurantDetail.statusText.set(context.getString(R.string.table_open))
        }
        else{
            restaurantDetail.userText.set(context.getString(R.string.total_player_registered))
            restaurantDetail.statusText.set(context.getString(R.string.table_close))
        }

        /*  restaurantDetailModel?.first_name?.let {
              restaurantDetail.name.set(it+" "+restaurantDetailModel?.last_name?.let { it })
          }*/
        tableList.addAll(restaurantDetailModel?.active_tables!!)
        tableAdapter.notifyDataSetChanged()
    }



    fun onClickFavourite(view: View){
        RetrofitClientBuilder.callService(context,true,"",object :RequestProcess<Response<AddFavouriteModel>>{
            override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<AddFavouriteModel> {
                return retrofitApi.addToFavourite(restaurantDetailModel?.id.toString(),AppClass.token)
            }

            override fun onResponse(res: Response<AddFavouriteModel>) {
                if (res.body()?.status == 200){
                    restaurantDetail.isFavourite.set(!restaurantDetail.isFavourite.get())
                    CommonMethods.showToast(context,res.body()?.message!!)

                } else
                    CommonMethods.showToast(context,"Something went wrong.")
            }

            override fun onException(message: String) {
                CommonMethods.showToast(context,message)
            }


        })
    }


    fun onClickArrivalTime(view: View){
        val timePicker: TimePickerDialog = TimePickerDialog(
            // pass the Context
            context,
            // listener to perform task
            // when time is picked
            detailActivity.timePickerDialogListener,
            // default hour when the time picker
            // dialog is opened
            12,
            // default minute when the time picker
            // dialog is opened
            10,
            // 24 hours time picker is
            // false (varies according to the region)
            false
        )

        // then after building the timepicker
        // dialog show the dialog to user
        timePicker.show()

    }


    fun onClickCancel(view: View){
        (context as Activity).onBackPressed()
    }

    fun onClickConfirm(view: View){
//        when {
          /*  people.isNullOrEmpty() -> {
                CommonMethods.showToast(context,"Please select number of people")
            }
            selectedTableList.isEmpty() -> {
                CommonMethods.showToast(context,"Please select table")
            }*/
//            else -> {
             /*   var ids = ""
                for (i in 0 until selectedTableList.size){
                    ids = ids+selectedTableList[i]+","
                }*/

        when {
            restaurantDetail.name.get()!!.trim().toString().isEmpty() -> {
                CommonMethods.showToast(context,context.getString(R.string.enter_your_name))

            }
            restaurantDetail.surname.get()!!.trim().toString().isEmpty() -> {
                CommonMethods.showToast(context,context.getString(R.string.enter_your_surname))
            }
            restaurantDetail.arrivalTime.get()!!.trim().toString().isEmpty() -> {
                CommonMethods.showToast(context,context.getString(R.string.select_arrival_time))
            }
            else -> {
                hashMap["OrderQueue[session_id]"] = AppClass.token
                hashMap["OrderQueue[email]"] = restaurantDetail.email.get()?.trim().toString()
                hashMap["OrderQueue[arrival_time]"] = restaurantDetail.arrivalTime.get()?.trim().toString()
                hashMap["OrderQueue[surname]"] = restaurantDetail.surname.get()?.trim().toString()
                hashMap["OrderQueue[contact_no]"] = restaurantDetail.phone.get()?.trim().toString()
                hashMap["OrderQueue[full_name]"] = restaurantDetail.name.get()?.trim().toString()
                hashMap["OrderQueue[device_type]"] = "1"
                hashMap["OrderQueue[language]"] = lang
                hashMap["OrderQueue[service_type]"] = "6"

                RetrofitClientBuilder.callService(context,true,"",object :RequestProcess<Response<CasinoQueueModel>>{
                    override suspend fun sendRequest(retrofitApi: RetrofitAPIs): Response<CasinoQueueModel> {
                        return retrofitApi.editDetail(restaurantDetailModel?.id.toString(),hashMap)
                    }

                    override fun onResponse(res: Response<CasinoQueueModel>) {
                        if (res.body()?.status == 200){
                            CommonMethods.showToast(context,res.body()?.message!!)
                            val casinoQueueModel = res.body()?.detail
                            casinoQueueModel?.message = restaurantDetailModel?.qr_scan_message
                            casinoQueueModel?.image = restaurantDetailModel?.profile_file
                            casinoQueueModel?.logo = restaurantDetailModel?.logo_file
                            casinoQueueModel?.name = restaurantDetailModel?.full_name

                            val bundle = Bundle()
                            bundle.putSerializable("casino_model",res.body()?.detail)

                            context.startActivity(Intent(context,StatusActivity::class.java).also {
                                it.putExtras(bundle)
                            })

    /*
                                context.startActivity(Intent(context,StatusActivity::class.java).putExtra("status","0")
                                    .putExtra("ticket_number", if (res.body()?.detail?.queue_no!=null)res.body()?.detail?.queue_no?.toString() else "")
                                    .putExtra("estimate_time", if (res.body()?.detail?.estimated_time!=null)res.body()?.detail?.estimated_time?.toString() else ""))
    */
                            (context as Activity).finishAffinity()
                        } else
                            CommonMethods.showToast(context,"Something went wrong.")
                    }

                    override fun onException(message: String) {
                        CommonMethods.showToast(context,message)
                    }


                })

    //            }
    //        }
            }
        }

    }


    override fun onSelectItem(type: String, value: String) {
        if (type == "table"){
            if (selectedTableList.contains(value)){
                selectedTableList.remove(value)
            }
            else{
                selectedTableList.add(value)
            }
        }
        else{
            people = value
        }
    }




    data class RestaurantDetail(
        var availableSeat:ObservableField<String> = ObservableField(""),
        var phone:ObservableField<String> = ObservableField(""),
        var userText:ObservableField<String> = ObservableField(""),
        var restName:ObservableField<String> = ObservableField(""),
        var name:ObservableField<String> = ObservableField(""),
        var surname:ObservableField<String> = ObservableField(""),
        var arrivalTime:ObservableField<String> = ObservableField(""),
        var email:ObservableField<String> = ObservableField(""),
        var banner:ObservableField<String> = ObservableField(""),
        var message:ObservableField<String> = ObservableField(""),
        var statusText:ObservableField<String> = ObservableField(""),
        var logo:ObservableField<String> = ObservableField(""),
        var isFavourite:ObservableBoolean = ObservableBoolean(false)
    )


}