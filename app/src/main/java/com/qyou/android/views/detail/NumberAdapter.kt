package com.qyou.android.views.detail

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.qyou.android.R
import com.qyou.android.databinding.NumberAdapterLayoutBinding

class NumberAdapter(
    var context: Context
) : RecyclerView.Adapter<NumberAdapter.ViewHolder>() {
    var list = listOf<String>("1","2","3","4","5","6","7","8")
    var pos = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val numberAdapterBinding:NumberAdapterLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.number_adapter_layout,
            parent,
            false
        )
        return ViewHolder(numberAdapterBinding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.setData(list[position])
        holder.itemView.animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
        holder.itemView.setOnClickListener{
            DetailActivity.detailActivityVM?.selectListener?.onSelectItem("number",list[position])
            pos = position
            notifyDataSetChanged()
        }
        if (pos == position)
            holder.text.background = ContextCompat.getDrawable(context,R.drawable.selected_circle)

        else
            holder.text.background = ContextCompat.getDrawable(context,R.drawable.circle_drawable)


    }

    override fun getItemViewType(position: Int): Int {
        return (position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ViewHolder(var numberAdapterBinding:NumberAdapterLayoutBinding) :
        RecyclerView.ViewHolder(numberAdapterBinding.root) {

        val text = numberAdapterBinding!!.text


        fun setData(number:String) {
            val numberAdapterVM = NumberAdapterVM(context,number)
            numberAdapterBinding.numberAdapterVM = numberAdapterVM
            numberAdapterBinding.executePendingBindings()
        }
    }
}
