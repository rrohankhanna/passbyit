package com.qyou.android.views.favourites

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.qyou.android.R
import com.qyou.android.databinding.FragmentFavouritesBinding
import com.qyou.android.views.pendingrequest.PendingRequestFactory
import com.qyou.android.views.pendingrequest.PendingRequestVM


class FavouritesFragment : Fragment() {

    private lateinit var favouritesVM: FavouritesVM
    private lateinit var fragmentFavouritesBinding: FragmentFavouritesBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        fragmentFavouritesBinding = FragmentFavouritesBinding.inflate(inflater)
        return fragmentFavouritesBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val favouriteFactory = FavouriteFactory(requireContext(),this)
        favouritesVM =
            ViewModelProviders.of(this, favouriteFactory).get(FavouritesVM::class.java)
        fragmentFavouritesBinding!!.favouriteVM = favouritesVM
    }


}