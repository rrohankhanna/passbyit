package com.qyou.android.views.favourites

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.qyou.android.views.pendingrequest.PendingRequest
import com.qyou.android.views.pendingrequest.PendingRequestVM
import java.lang.IllegalArgumentException

class FavouriteFactory(
    val context: Context,
    val favouritesFragment: FavouritesFragment
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(FavouritesVM::class.java)){
            return FavouritesVM(
                context,favouritesFragment
            ) as T
        }

        throw IllegalArgumentException("")
    }
}