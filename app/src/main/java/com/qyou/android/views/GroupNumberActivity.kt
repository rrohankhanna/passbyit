package com.qyou.android.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.qyou.android.R
import com.qyou.android.databinding.ActivityGroupNumberBinding

class GroupNumberActivity : AppCompatActivity() {
    private lateinit var groupNumberVM: GroupNumberVM
    private lateinit var activityGroupNumberBinding: ActivityGroupNumberBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityGroupNumberBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_group_number)
        groupNumberVM = GroupNumberVM(this, this)
        activityGroupNumberBinding!!.groupNumberVM = groupNumberVM
        activityGroupNumberBinding!!.groupNumberModel = groupNumberVM.restaurantDetail
    }
}